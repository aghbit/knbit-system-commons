from fabric.api import run, env
from fabric.context_managers import cd
from fabric.operations import put


def redeploy_knbit_system():
	with cd('/home/knbit-system/docker-compose'):
		put('docker-compose.yml', 'docker-compose.yml') 
		put('eventsbcpostgres', '.')
        	run('./docker-compose stop') 
		run('./docker-compose pull') 
		run('./docker-compose up -d')

